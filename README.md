# DCBM_solver

## Name
Degree-Corrected Block Model solver

## Description
This software solves the system of equation arising from a maximum-entropy Degree-Corrected Block Model, or from the Fitness-Corrected Block Model implemented as part of the USN framework at https://gitlab.com/cranic-group/usn
It allows to obtain the set of Lagrange's multipliers needed to compute the edge-probabilities of the DCBM/FCBM model.

## Dependencies
The software requires the Intel MKL library, the Gnu Scientific Library and the Open MP API installed.

## Usage
To compile just run compile.sh or
gcc -o dcbm_solver dcbm_solver.c -I/usr/include/mkl -O3 -lgsl -lm -lgslcblas -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -ldl
possibly replacing "-I/usr/include/mkl" with the path to your installation of the Intel MKL library

Once installed, launch it as follows
./dcbm_solver n_nodes n_blocks precision max_iter
where:
- n_nodes is the number of nodes in the network
- n_blocks is the number of blocks in which the nodes are partitioned
- precision and max_iter are stopping criteria: the program stops after max_iter iterations or as soon as the distance of the cost function at two consecutive iterations is less than precision
once launched, the program expects the system, in the form of n_nodes+(n_groups\*(n_groups+1)/2) input lines, formatted as follows:
- the first n_nodes lines must have the format 
     k b init
  where: k is the node's expected degree, possibly a float; b is the node's block, an int \<n_groups; init is the multiplier's initial value, possibly a random float in (0,1)
- the other n_groups\*(n_groups+1)/2 lines must have the format
     K init
  where: K is the expected number of edges between the two blocks, possibly a float; init is the multiplier's initial value, possibly a random float in (0,1)
  the first of these lines is for blocks 0 and 1, the second for blocks 0 and 2, and so forth till the last one that is for blocks n_groups-2 and n_groups-1

To test the program, you may run:
cat test_graph_system_145_5.csv | ./dcbm_solver 145 5 0.0001 1000

The program prints the solution on screen and writes some information on log.txt

## Support
For any question, open an issue on gitlab or contact us at cranic-info@iac.rm.cnr.it 

## License
All the code and materials are licensed in GPLv3, the complete license can be find in the LICENSE file.
